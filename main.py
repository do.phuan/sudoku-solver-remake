import cv2
import numpy as np
import argparse
import pytesseract as pt
from PIL import Image

font = cv2.FONT_HERSHEY_SIMPLEX
biggest = None
max_area = 0
max_contour = None


def import_image(path):
        image = cv2.imread(path)
        blurredImg = cv2.GaussianBlur(image, (15, 15), 0)
        gray = cv2.cvtColor(blurredImg, cv2.COLOR_BGR2GRAY)
        return gray

def normalize_image(gray):
        kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
        close = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel1)

        div = np.float32(gray)/(close)
        norm_image = np.uint8(cv2.normalize(div, div, 0, 255, cv2.NORM_MINMAX))
        return norm_image

def preprocessing(gray):
        norm_image = normalize_image(gray)
        processed_image = cv2.cvtColor(norm_image, cv2.COLOR_GRAY2BGR)

        return processed_image

def find_contours(image):
        threshold = cv2.adaptiveThreshold(image, 255,  0, 1,  19, 2)
        (_, contours,_) = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        return contours

def get_max_contour(contours):
        for i in contours:
                area = cv2.contourArea(i)
                if area > 1000 and area > max_area:
                        max_area = area
                        max_contour = i
        return max_contour

def draw_biggest_contour_on_mask(gray, max_contour):
        mask = np.zeros((gray.shape), np.uint8)
        cv2.drawContours(mask, [max_contour], 0, 255 , -1)
        cv2.drawContours(mask, [max_contour], 0, 0, 2)
        return mask

def detect_vertical_lines(gray, mask):
        norm_image = normalize_image(gray)
        res = cv2.bitwise_and(norm_image, mask)

        kernelx = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 10))

        dx = cv2.Sobel(res, cv2.CV_16S, 1, 0)
        dx = cv2.convertScaleAbs(dx)

        cv2.normalize(dx, dx, 0, 255, cv2.NORM_MINMAX)
        ret, close = cv2.threshold(dx, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        close = cv2.morphologyEx(close, cv2.MORPH_DILATE, kernelx, iterations = 1)

        (_, contour, _) = cv2.findContours(close, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for cnt in contour:
                x, y, w, h = cv2.boundingRect(cnt)
                if h/w > 5:
                        cv2.drawContours(close, [cnt], 0, 255, -1)
                else:
                        cv2.drawContours(close, [cnt], 0, 0, -1)
        close = cv2.morphologyEx(close, cv2.MORPH_CLOSE, None, iterations = 2)
        closex = close.copy()
        return closex

def detect_horizontal_lines(gray, mask):
        norm_image = normalize_image(gray)
        res = cv2.bitwise_and(norm_image, mask)

        kernely = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 2))
        
        dy = cv2.Sobel(res, cv2.CV_16S, 0, 2)
        dy = cv2.convertScaleAbs(dy)
        
        cv2.normalize(dy, dy, 0, 255, cv2.NORM_MINMAX)
        ret,close = cv2.threshold(dy, 0,  255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        close = cv2.morphologyEx(close, cv2.MORPH_DILATE, kernely)

        (_, contour, _) = cv2.findContours(close, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for cnt in contour:
                x, y, w, h = cv2.boundingRect(cnt)
                if w / h > 5:
                        cv2.drawContours(close, [cnt], 0, 255, -1)
                else:
                        cv2.drawContours(close, [cnt], 0, 0, -1)

        close = cv2.morphologyEx(close, cv2.MORPH_DILATE,  None, iterations = 2)
        closey = close.copy()
        return closey

def find_intersections(gray, mask):
        closex = detect_vertical_lines(gray, mask)
        closey = detect_horizontal_lines(gray, mask)
        bitwise_and_res = cv2.bitwise_and(closex, closey)
        return bitwise_and_res

def find_centroids_and_warp_image(image, intersections, processed_image):
        img = image.copy()
        centroids = []
        temp = []
        prevX = 0
        prevY = 0
        count = 0

        (_, contour, _) = cv2.findContours(intersections, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        for cnt in contour:
                mom = cv2.moments(cnt)
                (x, y) = int(mom['m10']/mom['m00']), int(mom['m01']/mom['m00'])
                centroids.append((x, y))
                count += 1

        centroids = np.array(centroids,dtype = np.float32)
        c = centroids.reshape((count, 2))
        sorted_centroids = c[np.argsort(c[:, 1])]

        count = 1

        for i in range(len(sorted_centroids)):
                x, y = sorted_centroids[i]
                if (((x-prevX)**2 + (y-prevY)**2)**0.5 > 20):
                        temp.append((x, y))
                        (prevX, prevY) = (x, y)
                        count += 1

        warp_arr = np.array(temp, dtype = np.float32)
        box = np.vstack([warp_arr[i * 10 : (i + 1) * 10][np.argsort(warp_arr[i * 10 : (i + 1) * 10, 0])] for i in xrange(10)])
        box_reshaped = box.reshape((10, 10, 2))

        output = np.zeros((450, 450, 3), np.uint8)

        # warp image
        for i, j in enumerate(box):
                ri = i/10
                ci = i%10
                if ci != 9 and ri != 9:
                        src = box_reshaped[ri:ri + 2, ci:ci + 2, :].reshape((4,2))
                        dst = np.array([[ci * 50,ri * 50], [(ci + 1)  50 - 1,ri * 50], ci * 50,(ri + 1) * 50 - 1], [(ci + 1) * 50 - 1,(ri + 1) * 50 - 1]], np.float32)
                        
                        retval = cv2.getPerspectiveTransform(src, dst)
                        
                        warp = cv2.warpPerspective(processed_image, retval, (450, 450))
                        output[ri * 50:(ri + 1) * 50 - 1, ci * 50:(ci + 1) * 50 - 1] = warp[ri * 50:(ri + 1) * 50 - 1, ci * 50:(ci + 1) * 50 - 1].copy()

        cv2.imwrite("FlatSudoku.jpg", output)
        return output


def  get_matrix(output):
        input = []
        is_zero = []
        matrix = []
        for i in range(0,9):
                for j in range(0,9):
                        x1 = j * (output.shape[0] / 9) + 5
                        x2 = (j + 1) * (output.shape[0] / 9) - 5
                        y1 = i * (output.shape[1] / 9) + 5
                        y2 = (i + 1) * (output.shape[1] / 9) - 5

                        element = output[y1:y2, x1:x2]
                        converted_image = Image.fromarray(element)
                        text = pt.image_to_string(converted_image, lang='eng', config=None)
                        text = re.sub('[^0-9]','',text)
                        if text == "":
                                text = "0"
                        matrix.append(int(text))
        return matrix

def find_next_cell_to_fill(grid, i, j):
        for x in range(i, 9):
                for y in range(j, 9):
                        if grid[x][y] == 0:
                                return x, y
        for x in range(0, 9):
                for y in range(0, 9):
                        if grid[x][y] == 0:
                                return x, y
        return -1, -1

def is_valid(grid, i, j, e):
    row_ok = all([e != grid[i][x] for x in range(9)])
    if row_ok:
        column_ok = all([e != grid[x][j] for x in range(9)])
        if column_ok:
            sec_top_x, sec_top_y = 3 * (i / 3), 3 * (j / 3)
            for x in range(sec_top_x, sec_top_x + 3):
                for y in range(sec_top_y, sec_top_y + 3):
                    if grid[x][y] == e:
                        return False
            return True
    return False

def solve_sudoku(matrix, i = 0, j = 0):
        i, j = find_next_cell_to_fill(grid, i, j)
        if i == -1:
                return True
        for e in range(1, 10):
                if is_valid(grid, i, j, e):
                        grid[i][j] = e
                        if solve_sudoku(grid, i, j):
                                return True
                        grid[i][j] = 0
        return False

if __name__ == "__main__":
        ap = argparse.ArgumentParser()
        ap.add_argument("-q", "--query", required = True, help = "Directory that contains input image")
        args = vars(ap.parse_args())

        gray = import_image(args["query"])
        
        processed_image = preprocessing(gray)
        
        contours = find_contours(processed_image)
        
        max_contour = get_max_contour(contours)

        mask = draw_biggest_contour_on_mask(gray, max_contour)

        intersections = find_intersections(gray, mask)

        warped_image = find_centroids_and_warp_image(gray, intersections, processed_image)

        matrix = get_matrix(warped_image)

        matrix = np.reshape(matrix, (9, -1))

        solve_sudoku(matrix)