# OCR Sudoku solver

This project aims to apply opencv scripts and python tesseract to extract sudoku matrix in images. Then this matrix will be solved by using backtracking algorithms.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

Install the following libraries:

- Opencv v2.x

- Tesseract and pytesseract

- Python 2.x or 3.x

- Pillow

- Numpy

- Npm (for linux and windows OS) or HomeBrew (for Macos)

## Setup

If you want to run this example for testing, just install support libs and apps as the following construction

> Mac OS

Install Homebrew

```shell
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Install Tesseract

```shell
$ brew install tesseract
```

Install Opencv

```shell
$ brew install opencv
```

Install Python3

```shell
$ brew install python3
```

Install pip and numpy

```shell
$ easy_install pip
$ pip install numpy
```

## Features

This project allows import an image and extract sudoku matrix (if available) in the image. The recognized sudoku matrix will be solved by backtracking algorithms. 

## Authors 

- Do Phu AN
- Luu Thanh SON
- Hoang Minh QUAN

## License

[![License](https://img.shields.io/:license-gnu-blue.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)

